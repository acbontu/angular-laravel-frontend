import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  listUrl = "http://localhost:8000/api/list";
  insertUrl = "http://localhost:8000/api/insert";
  constructor(private http: HttpClient) { }
  getData() {
    return this.http.get(this.listUrl);
  }

  saveData(data: any) {
    return this.http.post(this.insertUrl, data);
  }
}
