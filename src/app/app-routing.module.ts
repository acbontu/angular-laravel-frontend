import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './CRUD/create/create.component';
import { ListComponent } from './CRUD/list/list.component';
import { ShowComponent } from './CRUD/show/show.component';

const routes: Routes = [
  {
    component: CreateComponent,
    path: 'create'
  },
  {
    component: ListComponent,
    path: 'list'
  },
  {
    component: ShowComponent,
    path: 'update/:id'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
