import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../../Services/get-data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  dataList: any;
  constructor(private list: GetDataService) {
    this.list.getData().subscribe((data) => {
      this.dataList = data;
    })
  }

  ngOnInit(): void {
  }

}
