import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GetDataService } from '../../Services/get-data.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  constructor(private postData: GetDataService) { }

  ngOnInit(): void {
  }
  insertionForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    phone_number: new FormControl('', [Validators.required])
  });
  get name() {
    return this.insertionForm.get('name');
  }
  get email() {
    return this.insertionForm.get('email');
  }
  get phone_number() {
    return this.insertionForm.get('phone_number');
  }
  insertFormData() {
    this.postData.saveData(this.insertionForm.value).subscribe((result) => {
      console.warn()
    });
  }

}
